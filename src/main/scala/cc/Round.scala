package cc

/**
  * Created by mark on 16/04/2017.
  */
case class Round(r:Double) {
  def area()=scala.math.pow(r,2)*scala.math.Pi

  def circu= 2*r*scala.math.Pi

}
